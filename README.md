# README #
___I've temporarily removed the repository. It seems that I made some mistakes in the way that I used some Unity resources. Those fixes are in process. Stay tuned.___

This software is unsupported. Hey, it's free, right? I built it fine with Unity version 2017.2.0f3 (Personal Edition).
Developed by Leland Green solely by watching the (very much outdated) Unity tutorial for Space Shooter.
All graphics are available free of charge from the Unity Asset store. (All by Unity, except for one. Thanks to Adam Bielecki for the Milky Way skybox. You can find it here: https://www.assetstore.unity3d.com/en/#!/content/94001)

### This is to demonstrate how to take the Space Shooter Unity tutorial and convert it into a 3D app with full VR capabilities. ###

* This is the initial version. It's working.
* 0.0.7
* [Stay tuned] This may get better! (And why not see https://bitbucket.org/tutorials/markdowndemo?)

### How do I get set up? ###

* Install latest version of Unity. Clone this project and open the folder in Unity. 
* Open the project and import the Space Shooter Asset from the Unity Asset Store.
* Open the scene named Main in the "Assets->__Scenes" folder. 
* Optionally, click "Maximize on Play" in the scene (Unity) Game window.
* Click "Play" in Unity. 

### Optimizing ###

* Tested on a NVIDIA GTX 1080TI. Runs great there.
* If you really want to play on a lower-end system, you'll have to tweak the settings in the GameController object. Specifically, try spawn rates that are slower (now set to crazy-fast 0.001) and a smaller number of asteroids. Lowering things like that should help.
* You can also tweak the custom Delay objects, which control how often the AI on the enemy ship updates.

### Building ###
* I'm not going into details of building in Unity. I'm sorry, if you'd like an executable (app), submit an issue and I'll think about building one. No promises beyond that.
* Dependencies: None, really... builds with Unity.
* Deployment instructions: See Unity tutorials. I'm not going to support this. Sorry, this is for developers.

### Contribution guidelines ###

* I would consider any pull requests. Not gonna promise to accept them. If I like them and they fit with my plans, sure, why not? :-)

### Who do I talk to? ###

* Repo owner is Leland Green. Feel free to message on BitBucket, Facebook or anywhere you can find me.
* I'd like some team members to jump in here. You know who you are. ;-)
